Installation Order
==================

This README explain how to get PHP5 works with Microsoft SQL Server.
You can choose using FreeTDS or Microsoft SQL Server Native Client or both.


FreeTDS
=======

* unixODBC
* freetds
* php-pdo_dblib


NOTES
=====

* Default Slackware's ODBC library is libiodbc. unixODBC will replace some of
  libiodbc files. Please *reinstall* libiodbc if you uninstall unixODBC.

* If you don't like your stock libraries being replaced,
  then FreeTDS is a safe choice.
